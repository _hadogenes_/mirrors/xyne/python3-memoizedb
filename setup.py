#!/usr/bin/env python3

from distutils.core import setup
import time

setup(
  name='''MemoizeDB''',
  version=time.strftime('%Y.%m.%d.%H.%M.%S', time.gmtime(1490917129)),
  description='''Generic data retrieval memoizer that uses an sqlite database to cache data.''',
  author='''Xyne''',
  author_email='''ac xunilhcra enyx, backwards''',
  url='''http://xyne.archlinux.ca/projects/python3-memoizedb''',
  py_modules=['''MemoizeDB'''],
)
